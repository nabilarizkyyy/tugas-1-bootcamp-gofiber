package app

import (
	"errors"
)

func Multiply(a, b float32) float32 {
	return a * b
}

func IsInteger(value interface{}) (bool, error) {
	_, ok := value.(int)
	if ok {
		return true, nil
	} else {
		return false, errors.New("Nilai bukan merupakan integer")
	}
}

func IntToDayName(dayNumber int) string {
	days := []string{"senin", "selasa", "rabu", "kamis", "jumat", "sabtu", "minggu"}

	if dayNumber >= 1 && dayNumber <= 7 {
		return days[dayNumber-1]
	}

	return ""
}

func DayNameToInt(dayName string) int {
	hari := map[string]int{
		"senin":  1,
		"selasa": 2,
		"rabu":   3,
		"kamis":  4,
		"jumat":  5,
		"sabtu":  6,
		"minggu": 7,
	}

	return hari[dayName]
}

func IsiHello(str *string) {
	if *str == "" {
		*str = "hello world"
	}
}
